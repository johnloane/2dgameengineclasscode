var http = require('http');
var io = require('socket.io');
var fs = require('fs');
server = http.createServer();
server.listen(49800);
var socket = io.listen(server);

socket.on('connection', function(client)
{
	client.on(
		'life lost',
		function(data)
		{
			fs.appendFile('lives-lost.txt', '(' + data.left.toFixed(0)
				+ ', ' + data.top.toFixed(0) + ')',
			function(err)
			{
				if(err) throw err;
			});
		});
});