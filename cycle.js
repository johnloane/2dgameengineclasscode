/*CYCLE: For sprites that have a spritesheet artist, this
behaviour just advances the sprite artist through the sprites 
frames. Can specify the duration that each frame will be displayed
and you can specify the interval between loops*/ 

CycleBehaviour = function(duration, interval)
{
	this.duration = duration || 1000;
	this.lastAdvance = 0;
	this.interval = interval;
};

CycleBehaviour.prototype =
{
	execute: function(sprite, now, fps, context, lastAnimationFrameTime)
	{
		if(this.lastAdvance === 0) //First time only
		{
			this.lastAdvance = now;
		}
		if(this.interval && sprite.artist.cellIndex === 0)
		{
			if(now - this.lastAdvance > this.interval)
			{
				sprite.artist.advance();
				this.lastAdvance = now;
			}
		}
		else if(now - this.lastAdvance > this.duration)
		{
			sprite.artist.advance();
			this.lastAdvance = now;
		}
		

	}
};