//snailBait a game designed by David Geary
//See link to his book on Moodle
var SnailBait = function()
{
	this.canvas = document.getElementById('game-canvas'),
	this.context = this.canvas.getContext('2d'),
	this.fpsElement = document.getElementById('fps'),
	this.gameStarted = false,
	this.scoreElement = document.getElementById('score'),

   // Pixels and meters.................................................

   this.CANVAS_WIDTH_IN_METERS = 13,  // Proportional to sprite sizes

   this.PIXELS_PER_METER = this.canvas.width / 
                           this.CANVAS_WIDTH_IN_METERS,

   // Gravity...........................................................

   this.GRAVITY_FORCE = 9.81, // m/s/s

   // Lives.............................................................

   this.MAX_NUMBER_OF_LIVES = 3;
   this.lives = this.MAX_NUMBER_OF_LIVES;

   //Mobile
   this.mobileInstructionsVisible = false,
   this.mobileStartToast = 
            document.getElementById('snailbait-mobile-start-toast'),
   this.mobileWelcomeToast = 
            document.getElementById('snailbait-mobile-welcome-toast'),
   this.welcomeStartLink = 
            document.getElementById('snailbait-welcome-start-link'),
   this.showHowLink = 
            document.getElementById('snailbait-show-how-link'),
   this.mobileStartLink = 
            document.getElementById('snailbait-mobile-start-link'),


   //Directions
   this.LEFT = 1,
   this.RIGHT = 2,

   //Pacing
   this.BUTTON_PACE_VELOCITY = 50,
   this.SNAIL_PACE_VELOCITY = 100,

	//Constants 
	this.PLATFORM_HEIGHT = 8,
	this.PLATFORM_STROKE_WIDTH = 2,
	this.PLATFORM_STROKE_STYLE = 'rgb(0,0,0)', //black
	this.SHORT_DELAY = 50,
   this.timeRate = 1.0,
   this.timeSystem = new TimeSystem(),

   this.RUNNER_EXPLOSION_DURATION = 500,
   this.BAD_GUYS_EXPLOSION_DURATION = 1500,

	this.RUNNER_LEFT = 50,
	this.STARTING_RUNNER_TRACK = 1,
	this.TRANSPARENT = 0,
	this.OPAQUE = 1.0,

   //Socket code to connect to server
   this.serverAvailable = true;
   try
   {
      this.socket = new io.connect('http://localhost:49800');
   }
   catch(err)
   {
      this.serverAvailable = false;
   }
   

	//Track baselines
	this.TRACK_1_BASELINE = 323,
	this.TRACK_2_BASELINE = 223,
	this.TRACK_3_BASELINE = 123,

	//Scrolling constants
	this.BACKGROUND_VELOCITY = 42,
	this.STARTING_BACKGROUND_VELOCITY = 0,
	this.STARTING_BACKGROUND_OFFSET = 0,
	this.STARTING_PLATFORM_OFFSET = 0,
	this.PLATFORM_VELOCITY_MULTIPLIER = 4,

	//Imagethis.s
	//this.background = new Image(),
	//this.runnerImage = new Image(),
   this.spritesheet = new Image(),
   this.spriteOffset = 0,
   this.lastAdvanceTime = 0,
   this.RUN_ANIMATION_RATE = 10,
	//this.snailAnimatedGif = new Image(),

   //Background width and height
   this.BACKGROUND_WIDTH = 1102,
   this.BACKGROUND_HEIGHT = 400,

	//Loading screen
	this.loadingElement = document.getElementById('loading'),
	this.loadingTitleElement = document.getElementById('loading-title'),
	this.snailAnimatedGifElement = document.getElementById('loading-animated-gif'),

	//Toast
	this.toastElement = document.getElementById('toast'),

	//Instructions
	this.instructionsElement = document.getElementById('instructions'),

	//Copyright
	this.copyrightElement = document.getElementById('copyright'),

	//Sound and music
	this.soundAndMusicElement = document.getElementById('sound-and-music'),
   this.musicElement = document.getElementById('snailbait-music'),
   this.musicElement.volume = 0.1,
   this.musicCheckboxElement = document.getElementById('music-checkbox'),
   this.soundCheckboxElement = document.getElementById('sound-checkbox'),
   this.musicOn = this.musicCheckboxElement.checked,
   this.audioSprites= document.getElementById('snailbait-audio-sprites'),
   this.soundOn = this.soundCheckboxElement.checked,

   this.audioChannels = [
      {playing: false, audio: this.audioSprites,},
      {playing: false, audio: null,},
      {playing: false, audio: null,},
      {playing: false, audio: null}],

   this.audioSpriteCountdown = this.audioChannels.length - 1,
   this.graphicsReady = false,
   //Sounds
   this.cannonSound = 
   {
      position: 7.7, //seconds
      duration: 1031, //milliseconds
      volume: 0.5
   },

   this.coinSound = 
   {
      position: 7.1,
      duration: 588,
      volume: 0.5
   },

   this.electricityFlowingSound = 
   {
      position: 1.03,
      duration: 1753,
      volume: 0.5
   },

   this.explosionSound = 
   {
      position: 4.3,
      duration: 760,
      volume: 1.0
   },

   this.pianoSound = 
   {
      position: 5.6,
      duration: 395,
      volume: 0.5
   },

   this.thudSound = 
   {
      position: 3.1,
      duration: 809,
      volume: 1.0
   },


	//States
	this.paused = false,
	this.PAUSED_CHECK_INTERVAL = 200,
	this.windowHasFocus = true,
	this.countdownInProgress = false,

	//Time
	this.lastAnimationFrameTime = 0,
	this.fps = 60,
	this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
	this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
	this.platformOffset = this.STARTING_PLATFORM_OFFSET,
	this.lastFpsUpdateTime = 0,
	this.platformVelocity,
	this.pauseStartTime,

	//Sprite Sheet cells
	this.RUNNER_CELLS_WIDTH = 50;
	this.RUNNER_CELLS_HEIGHT = 54;

	this.BAT_CELLS_HEIGHT = 34;

	this.BEE_CELLS_WIDTH = 50;
	this.BEE_CELLS_HEIGHT = 50;

	this.BUTTON_CELLS_HEIGHT = 20;
	this.BUTTON_CELLS_WIDTH = 31;

	this.COIN_CELLS_WIDTH = 30;
	this.COIN_CELLS_HEIGHT = 30;

	this.EXPLOSION_CELLS_HEIGHT = 62;

	this.RUBY_CELLS_HEIGHT = 30;
	this.RUBY_CELLS_WIDTH = 35;

	this.SAPPHIRE_CELLS_HEIGHT = 30;
	this.SAPPHIRE_CELLS_WIDTH = 35;

	this.SNAILBOMB_CELLS_WIDTH = 20;
	this.SNAILBOMB_CELLS_HEIGHT = 20;

	this.SNAIL_CELLS_WIDTH = 64;
	this.SNAIL_CELLS_HEIGHT = 34;

	this.batCells = [
	{left: 3, top: 0, width: 36, height: this.BAT_CELLS_HEIGHT},
	{left: 41, top: 0, width: 46, height: this.BAT_CELLS_HEIGHT},
	{left: 93, top: 0, width: 36, height: this.BAT_CELLS_HEIGHT},
	{left: 132, top: 0, width: 46, height: this.BAT_CELLS_HEIGHT},
	];

	this.batRedEyeCells = [
	{left: 185, top: 0, width: 36, height: this.BAT_CELLS_HEIGHT},
	{left: 222, top: 0, width: 46, height: this.BAT_CELLS_HEIGHT},
	{left: 273, top: 0, width: 36, height: this.BAT_CELLS_HEIGHT},
	{left: 313, top: 0, width: 46, height: this.BAT_CELLS_HEIGHT},
	];

	this.beeCells = [
	{left: 5, top: 234, width: 36, height: this.BEE_CELLS_HEIGHT, width: this.BEE_CELLS_WIDTH},
	{left: 75, top: 234, width: 46, height: this.BEE_CELLS_HEIGHT, width: this.BEE_CELLS_WIDTH},
	{left: 145, top: 234, width: 36, height: this.BEE_CELLS_HEIGHT, width: this.BEE_CELLS_WIDTH},
	];

	this.blueCoinCells = [
      { left: 5, top: 540, width: this.COIN_CELLS_WIDTH, 
                           height: this.COIN_CELLS_HEIGHT },

      { left: 5 + this.COIN_CELLS_WIDTH, top: 540,
        width: this.COIN_CELLS_WIDTH, 
        height: this.COIN_CELLS_HEIGHT }
   ];

   this.explosionCells = [
      { left: 3,   top: 48, 
        width: 52, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 63,  top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 146, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 233, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 308, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 392, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 473, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT }
   ];

   // Sprite sheet cells................................................

   this.blueButtonCells = [
      { left: 10,   top: 192, width: this.BUTTON_CELLS_WIDTH,
                            height: this.BUTTON_CELLS_HEIGHT },

      { left: 53,  top: 192, width: this.BUTTON_CELLS_WIDTH, 
                            height: this.BUTTON_CELLS_HEIGHT }
   ];

   this.goldCoinCells = [
      { left: 65, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 96, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 128, top: 540, width: this.COIN_CELLS_WIDTH, 
                             height: this.COIN_CELLS_HEIGHT },
   ];

   this.goldButtonCells = [
      { left: 90,   top: 190, width: this.BUTTON_CELLS_WIDTH,
                              height: this.BUTTON_CELLS_HEIGHT },

      { left: 132,  top: 190, width: this.BUTTON_CELLS_WIDTH,
                              height: this.BUTTON_CELLS_HEIGHT }
   ];

   this.sapphireCells = [
      { left: 185,   top: 138, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 220,  top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 258,  top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 294, top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 331, top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT }
   ];

   this.runnerCellsRight = [
      { left: 414, top: 385, 
        width: 47, height: this.RUNNER_CELLS_HEIGHT },

      { left: 362, top: 385, 
         width: 44, height: this.RUNNER_CELLS_HEIGHT },

      { left: 314, top: 385, 
         width: 39, height: this.RUNNER_CELLS_HEIGHT },

      { left: 265, top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 205, top: 385, 
         width: 49, height: this.RUNNER_CELLS_HEIGHT },

      { left: 150, top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 96,  top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 45,  top: 385, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },

      { left: 0,   top: 385, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT }
   ],

   this.runnerCellsLeft = [
      { left: 0,   top: 305, 
         width: 47, height: this.RUNNER_CELLS_HEIGHT },

      { left: 55,  top: 305, 
         width: 44, height: this.RUNNER_CELLS_HEIGHT },

      { left: 107, top: 305, 
         width: 39, height: this.RUNNER_CELLS_HEIGHT },

      { left: 152, top: 305, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 208, top: 305, 
         width: 49, height: this.RUNNER_CELLS_HEIGHT },

      { left: 265, top: 305, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 320, top: 305, 
         width: 42, height: this.RUNNER_CELLS_HEIGHT },

      { left: 380, top: 305, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },

      { left: 425, top: 305, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },
   ],

   this.rubyCells = [
      { left: 3,   top: 138, width: this.RUBY_CELLS_WIDTH,
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 39,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 76,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 112, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 148, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT }
   ];

   this.snailBombCells = [
      { left: 40, top: 512, width: 30, height: 20 },
      { left: 2, top: 512, width: 30, height: 20 }
   ];

   this.snailCells = [
      { left: 142, top: 466, width: this.SNAIL_CELLS_WIDTH,
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 75,  top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 2,   top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT },
   ]; 

   // Sprite data.......................................................

   this.batData = [
      { left: 85,  
         top: this.TRACK_2_BASELINE - 1.5*this.BAT_CELLS_HEIGHT },

      { left: 620,  
         top: this.TRACK_3_BASELINE },

      { left: 904,  
         top: this.TRACK_3_BASELINE - 3*this.BAT_CELLS_HEIGHT },

      { left: 1150, 
         top: this.TRACK_2_BASELINE - 3*this.BAT_CELLS_HEIGHT },

      { left: 1720, 
         top: this.TRACK_2_BASELINE - 2*this.BAT_CELLS_HEIGHT },

      { left: 1960, 
         top: this.TRACK_3_BASELINE - this.BAT_CELLS_HEIGHT }, 

      { left: 2200, 
         top: this.TRACK_3_BASELINE - this.BAT_CELLS_HEIGHT },

      { left: 2380, 
         top: this.TRACK_3_BASELINE - 2*this.BAT_CELLS_HEIGHT },
   ];
   
   this.beeData = [
      { left: 200,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT*1.5 },
      { left: 350,  
         top: this.TRACK_2_BASELINE - this.BEE_CELLS_HEIGHT*1.5 },
      { left: 550,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT },
      { left: 750,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT*1.5 },

      { left: 924,  
         top: this.TRACK_2_BASELINE - this.BEE_CELLS_HEIGHT*1.75 },

      { left: 1500, top: 225 },
      { left: 1600, top: 115 },
      { left: 2225, top: 125 },
      { left: 2295, top: 275 },
      { left: 2450, top: 275 },
   ];
   
   this.buttonData = [
      { platformIndex: 2 },
      { platformIndex: 12 },
   ];

   this.coinData = [
      { left: 270,  
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 489,  
         top: this.TRACK_3_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 620,  
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 833,  
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1050, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1450, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1670, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1870, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1930, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2200, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2320, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2360, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 
   ];  

   // Platforms.........................................................

   this.platformData = [
      // Screen 1.......................................................
      {
         left:      10,
         width:     230,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(150,190,255)',
         opacity:   1.0,
         track:     1,
         pulsate:   false,
      },

      {  left:      250,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(150,190,255)',
         opacity:   1.0,
         track:     2,
         pulsate:   false,
      },

      {  left:      400,
         width:     125,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(250,0,0)',
         opacity:   1.0,
         track:     3,
         pulsate:   true
      },

      {  left:      633,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   1.0,
         track:     1,
         pulsate:   false,
      },

      // Screen 2.......................................................
               
      {  left:      810,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,0)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1025,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1200,
         width:     125,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     3,
         pulsate:   false
      },

      {  left:      1400,
         width:     180,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   1.0,
         track:     1,
         pulsate:   false,
      },

      // Screen 3.......................................................
               
      {  left:      1625,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,0)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1800,
         width:     250,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   1.0,
         track:     1,
         pulsate:   false
      },

      {  left:      2000,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,80)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      2100,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     3,
      },


      // Screen 4.......................................................

      {  left:      2269,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'gold',
         opacity:   1.0,
         track:     1,
      },

      {  left:      2500,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: '#2b950a',
         opacity:   1.0,
         track:     2,
         snail:     true
      },
   ];

   this.sapphireData = [
      { left: 140,  
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 880,  
         top: this.TRACK_2_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 1100, 
         top: this.TRACK_2_BASELINE - this.SAPPHIRE_CELLS_HEIGHT }, 

      { left: 1475, 
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 2400, 
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },
   ];

   this.rubyData = [
      { left: 690,  
         top: this.TRACK_1_BASELINE - this.RUBY_CELLS_HEIGHT },

      { left: 1700, 
         top: this.TRACK_2_BASELINE - this.RUBY_CELLS_HEIGHT },

      { left: 2056, 
         top: this.TRACK_2_BASELINE - this.RUBY_CELLS_HEIGHT },
   ];

   this.smokingHoleData = [
      { left: 248,  top: this.TRACK_2_BASELINE - 22 },
      { left: 688,  top: this.TRACK_3_BASELINE + 5 },
      { left: 1352,  top: this.TRACK_2_BASELINE - 18 },
   ];
   
   this.snailData = [
      { platformIndex: 13 },
   ];

	//Sprites
	this.bats = [];
	this.bees = [];
	this.buttons = [];
	this.coins = [];
	this.platforms = [];
	this.rubies = [];
	this.sapphires = [];
	this.snails = [];

	this.sprites = [];

	this.platformArtist = 
	{
		draw: function(sprite, context)
		{
			var PLATFORM_STROKE_WIDTH = 1.0,
				PLATFORM_STROKE_STYLE = 'black',
				top;
			top = snailBait.calculatePlatformTop(sprite.track);
			context.lineWidth = PLATFORM_STROKE_WIDTH;
			context.strokeStyle = PLATFORM_STROKE_STYLE;
			context.fillStyle = sprite.fillStyle;

			context.strokeRect(sprite.left, top, sprite.width, 
			sprite.height);
			context.fillRect(sprite.left, top, sprite.width, 
			sprite.height);
		},
	};

   //Sprite behaviours
   this.runBehaviour = 
   {
      lastAdvanceTime: 0,
      execute: function(sprite, now, fps, context, 
            lastAnimationFrameTime)
      {
         if(sprite.runAnimationRate === 0)
         {
            return;
         }
         if(this.lastAdvanceTime === 0)
         {
            this.lastAdvanceTime = now;
         }
         else if(now-this.lastAdvanceTime > 
            1000/sprite.runAnimationRate)
         {
            sprite.artist.advance();
            this.lastAdvanceTime = now;
         }
      }
   };

      // Runner explosions.................................................
   this.runnerExplodeBehaviour = new CellSwitchBehaviour(
      this.explosionCells,
      this.RUNNER_EXPLOSION_DURATION,

      function (sprite, now, fps) { // Trigger
         return sprite.exploding;
      },
                                                
      function (sprite, animator) { // Callback
         sprite.exploding = false;
      }
   );

   // Bad guy explosions................................................

   this.badGuyExplodeBehaviour = new CellSwitchBehaviour(
      this.explosionCells,
      this.BAD_GUYS_EXPLOSION_DURATION,

      function (sprite, now, fps) { // Trigger
         return sprite.exploding;
      },
                                                
      function (sprite, animator) { // Callback
         sprite.exploding = false;
      }
   );
   //Jump Behaviour
   this.jumpBehaviour = 
   {
      pause: function(sprite, now)
      {
         if(sprite.ascendTimer.isRunning())
         {
            sprite.ascendTimer.pause(now);
         }
         else if(sprite.descendTimer.isRunning())
         {
            sprite.descendTimer.pause(now);
         }
      },

      unpause: function(sprite, now)
      {
         if(sprite.ascendTimer.isRunning())
         {
            sprite.ascendTimer.unpause(now);
         }
         else if(sprite.descendTimer.isRunning())
         {
            sprite.descendTimer.unpause(now);
         }
      },

      isAscending: function(sprite)
      {
         return sprite.ascendTimer.isRunning();
      },

      ascend: function(sprite, now)
      {
         var elapsed = sprite.ascendTimer.getElapsedTime(now),
             deltaY = elapsed / (sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
         sprite.top = sprite.verticalLaunchPosition - deltaY;
      },

      isDoneAscending: function(sprite, now)
      {
         return sprite.ascendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishAscent: function(sprite, now)
      {
         sprite.jumpApex = sprite.top;
         sprite.ascendTimer.stop(now);
         sprite.descendTimer.start(now);
      },

      isDescending: function(sprite)
      {
         return sprite.descendTimer.isRunning();
      },

      descend: function(sprite, now)
      {
         var elapsed = sprite.descendTimer.getElapsedTime(now),
         deltaY = elapsed / (sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
         sprite.top = sprite.jumpApex + deltaY;
      },

      isDoneDescending: function(sprite, now)
      {
         return sprite.descendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishDescent: function(sprite, now)
      {
         sprite.stopJumping();

         if (snailBait.platformUnderneath(sprite)) {
            sprite.top = sprite.verticalLaunchPosition;
         }
         else {
            sprite.fall(snailBait.GRAVITY_FORCE *
               (sprite.descendTimer.getElapsedTime(now)/1000) *
               snailBait.PIXELS_PER_METER);
         }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
         if(!sprite.jumping || sprite.track === 3)
         {
            return;
         }
         if(this.isAscending(sprite))
         {
            if(!this.isDoneAscending(sprite, now))
            {
               this.ascend(sprite, now);
            }
            else
               this.finishAscent(sprite, now);
         }
         else if(this.isDescending(sprite))
         {
            if(!this.isDoneDescending(sprite, now))
            {
               this.descend(sprite, now);
            }
            else
               this.finishDescent(sprite, now);
         }
      }
   };

   this.collideBehaviour=
   {
      isCandidateForCollision: function(sprite, otherSprite)
      {
         var s, o;
         s = sprite.calculateCollisionRectangle(),
         o = otherSprite.calculateCollisionRectangle();
         return o.left < s.right && sprite !== otherSprite &&
               sprite.visible && otherSprite.visible && !sprite.exploding && 
               !otherSprite.exploding;
      },

      didCollide: function(sprite, otherSprite, context)
      {
         var s, o;
         s = sprite.calculateCollisionRectangle(),
         o = otherSprite.calculateCollisionRectangle();

         //Check whether any of the runner's four corners or centre
         //lie in the other sprites bounding box
         context.beginPath();
         context.rect(o.left, o.top, o.right-o.left, o.bottom-o.top);
         return context.isPointInPath(s.left, s.top) ||
                context.isPointInPath(s.right, s.top) ||
                context.isPointInPath(s.centreX, s.centreY) ||
                context.isPointInPath(s.left, s.bottom) ||
                context.isPointInPath(s.right, s.bottom);
      },

      processBadGuyCollision: function(sprite)
      {
         //END GAME or decrease lives
         snailBait.explode(sprite);
         if(sprite.type === 'runner')
         {
            snailBait.loseLife();
         }
      },

      processPlatformCollisionDuringJump: function(sprite, platform)
      {
         var isDescending = sprite.descendTimer.isRunning();

         sprite.stopJumping();

         if (isDescending) { 
            sprite.track = platform.track;
            sprite.top = snailBait.calculatePlatformTop(sprite.track) - 
                         sprite.height;
         }
         else { // Collided with platform while ascending
            sprite.fall();
         }
      },

      processCollision: function(sprite, otherSprite)
      {
         if(sprite.jumping && 'platform' === otherSprite.type)
         {
            this.processPlatformCollisionDuringJump(sprite, otherSprite);
         }
         else if('coin' === otherSprite.type || 'sapphire' === otherSprite.type
            ||'ruby' === otherSprite.type || 'snail' === otherSprite.type 
            ||'snailBomb' === otherSprite.type)
         {
            otherSprite.visible = false;
            snailBait.playSound(snailBait.pianoSound);
         }
         if('bat' === otherSprite.type || 'bee' === otherSprite.type)
         {
            this.processBadGuyCollision(sprite);
         }
      },
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
         var otherSprite;
         for(var i=0; i < snailBait.sprites.length; ++i)
         {
            otherSprite = snailBait.sprites[i];
            if(this.isCandidateForCollision(sprite, otherSprite))
            {
               if(this.didCollide(sprite, otherSprite, context))
               {
                  this.processCollision(sprite, otherSprite);
               }
            }
         }
      }
   };

   //Pacing on platforms
   this.paceBehaviour = 
   {
      setDirection: function(sprite)
      {
         var sRight = sprite.left + sprite.width,
             pRight = sprite.platform.left + 
             sprite.platform.width;

         if(sprite.direction === undefined)
         {
            sprite.direction = snailBait.RIGHT;
         }
         if(sRight > pRight && sprite.direction === snailBait.RIGHT)
         {
            sprite.direction = snailBait.LEFT;
         }
         else if(sprite.left < sprite.platform.left && sprite.direction === snailBait.LEFT)
         {
            sprite.direction = snailBait.RIGHT;
         }
      },

      setPosition: function(sprite, now, lastAnimationFrameTime)
      {
         var pixelsToMove = sprite.velocityX * 
                              (now-lastAnimationFrameTime)/1000;
         if(sprite.direction === snailBait.RIGHT)
         {
            sprite.left += pixelsToMove;
         }                    
         else
         {
            sprite.left -= pixelsToMove;
         }

      },
      execute: function(sprite, now, fps,context, 
         lastAnimationFrameTime)
      {
         this.setDirection(sprite);
         this.setPosition(sprite, now, lastAnimationFrameTime);
      }
   };

   this.snailShootBehaviour = 
   {
      execute: function(sprite, now, fps,context, 
         lastAnimationFrameTime)
      {
         var bomb = sprite.bomb,
            MOUTH_OPEN_CELL = 2;
         if(!snailBait.isSpriteInView(sprite))
         {
            return;
         }
         if(!bomb.visible && sprite.artist.cellIndex === MOUTH_OPEN_CELL)
         {
            bomb.left = sprite.left;
            bomb.visible = true;
            snailBait.playSound(snailBait.cannonSound);
         }
      }
   };

   this.snailBombMoveBehaviour = 
   {
      execute: function(sprite, now, fps,context, 
         lastAnimationFrameTime)
      {
         var SNAIL_BOMB_VELOCITY = 550;
         if(sprite.left + sprite.width > sprite.hOffset
            && sprite.left + sprite.width < sprite.hOffset + 
            sprite.width)
         {
            sprite.visible = false;
         }
         else
         {
            sprite.left -= SNAIL_BOMB_VELOCITY*
            ((now-lastAnimationFrameTime)/1000);
         }
      }
   };

   // Runner's fall behaviour............................................

   this.fallBehaviour = {
      pause: function (sprite, now) {
         sprite.fallTimer.pause(now);
      },

      unpause: function (sprite, now) {
         sprite.fallTimer.unpause(now);
      },
      
      isOutOfPlay: function (sprite) {
         return sprite.top > snailBait.canvas.height;
      },

      setSpriteVelocity: function (sprite, now) {
         sprite.velocityY = 
            sprite.initialVelocityY + snailBait.GRAVITY_FORCE *
            (sprite.fallTimer.getElapsedTime(now)/1000) *
            snailBait.PIXELS_PER_METER;
      },

      calculateVerticalDrop: function (sprite, now, 
                                       lastAnimationFrameTime) {
         return sprite.velocityY * (now - lastAnimationFrameTime) / 1000;
      },

      willFallBelowCurrentTrack: function (sprite, dropDistance) {
         return sprite.top + sprite.height + dropDistance >
                snailBait.calculatePlatformTop(sprite.track);
      },

      fallOnPlatform: function (sprite) {
         sprite.stopFalling();
         snailBait.putSpriteOnTrack(sprite, sprite.track);
      },

      moveDown: function (sprite, now, lastAnimationFrameTime) {
         var dropDistance;

         this.setSpriteVelocity(sprite, now);

         dropDistance = this.calculateVerticalDrop(
                           sprite, now, lastAnimationFrameTime);

         if ( ! this.willFallBelowCurrentTrack(sprite, dropDistance)) {
            sprite.top += dropDistance; 
         }
         else { // will fall below current track
            if (snailBait.platformUnderneath(sprite)) { // collision detection
               this.fallOnPlatform(sprite);
               sprite.stopFalling();
            }
            else {
               sprite.track--;
               sprite.top += dropDistance;
            }
         }
      },         

      execute: function (sprite, now, fps, context, 
                         lastAnimationFrameTime) {
         if ( ! sprite.falling) {
            if ( ! sprite.jumping && 
                 ! snailBait.platformUnderneath(sprite)) {
               sprite.fall();
            }
         }
         else { // falling
            if (this.isOutOfPlay(sprite) || sprite.exploding) {
               sprite.stopFalling();

               if (this.isOutOfPlay(sprite)) {
                  snailBait.loseLife();
               }
            }
            else { // not out of play or exploding
               this.moveDown(sprite, now, lastAnimationFrameTime);
            }
         }
      }
   };

};
//Launch the game
SnailBait.prototype = {

   createSprites: function()
   {
      this.createPlatformSprites(),
      this.createBatSprites(),
      this.createBeeSprites(),
      this.createButtonSprites(),
      this.createCoinSprites(),
      this.createRunnerSprite(),
      this.createRubySprites(),
      this.createSapphireSprites(),
      this.createSnailSprites(),

      this.initializeSprites();
      this.addSpritesToSpriteArray();
   },

   loseLife: function()
   {
      var TRANSITION_DURATION = 3000;

      this.lives--;
      this.startLifeTransition(snailBait.RUNNER_EXPLOSION_DURATION);

      setTimeout( function () { // After the explosion
         snailBait.endLifeTransition();
      }, TRANSITION_DURATION); 
   },

   startLifeTransition: function (slowMotionDelay) {
      var CANVAS_TRANSITION_OPACITY = 0.05,
          SLOW_MOTION_RATE = 0.1;

      this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
      this.playing = false;

      setTimeout( function () {
         snailBait.setTimeRate(SLOW_MOTION_RATE);
         snailBait.runner.visible = false;
      }, slowMotionDelay);
   },

   endLifeTransition: function () {
      var TIME_RESET_DELAY = 1000,
          RUN_DELAY = 500;

      snailBait.reset();

      setTimeout( function () { // Reset the time
         snailBait.setTimeRate(1.0);

         setTimeout( function () { // Stop running
            snailBait.runner.runAnimationRate = 0;
            snailBait.playing = true;
         }, RUN_DELAY);
      }, TIME_RESET_DELAY);
   },

      resetRunner: function () {
      this.runner.left      = snailBait.RUNNER_LEFT;
      this.runner.track     = 3;
      this.runner.hOffset   = 0;
      this.runner.visible   = true;
      this.runner.exploding = false;
      this.runner.jumping   = false;
      this.runner.top       = this.calculatePlatformTop(3) -
                            this.runner.height;

      this.runner.artist.cells     = this.runnerCellsRight;
      this.runner.artist.cellIndex = 0;
   },

   resetOffsets: function () {
      this.bgVelocity       = 0;
      this.backgroundOffset = 0;
      this.platformOffset   = 0;
      this.spriteOffset     = 0;
   },

   makeAllSpritesVisible: function () {
      for (var i=0; i < this.sprites.length; ++i) {
         this.sprites[i].visible = true; 
      }
   },

   reset: function () {
      this.resetOffsets();
      this.resetRunner();
      this.makeAllSpritesVisible();
      this.canvas.style.opacity = 1.0;
   },

   positionSprites: function (sprites, spriteData) {
      var sprite;

      for (var i = 0; i < sprites.length; ++i) {
         sprite = sprites[i];

         if (spriteData[i].platformIndex) { 
            this.putSpriteOnPlatform(sprite,
               this.platforms[spriteData[i].platformIndex]);
         }
         else {
            sprite.top  = spriteData[i].top;
            sprite.left = spriteData[i].left;
         }
      }
   },
   
   initializeSprites: function() {  
      this.positionSprites(this.bats,      this.batData);
      this.positionSprites(this.bees,      this.beeData);
      this.positionSprites(this.buttons,   this.buttonData);
      this.positionSprites(this.coins,     this.coinData);
      this.positionSprites(this.rubies,    this.rubyData);
      this.positionSprites(this.sapphires, this.sapphireData);
      this.positionSprites(this.snails,    this.snailData);

      this.armSnails();
      this.equipRunner();
   },

   equipRunnerForFalling: function () {
      this.runner.fallTimer = new AnimationTimer();

      this.runner.fall = function (initialVelocity) {
         this.falling = true;
         this.velocityY = initialVelocity || 0;
         this.initialVelocityY = initialVelocity || 0;
         this.fallTimer.start(
            snailBait.timeSystem.calculateGameTime());
      };

      this.runner.stopFalling = function () {
         this.falling = false;
         this.velocityY = 0;
         this.fallTimer.stop(
            snailBait.timeSystem.calculateGameTime());
      };
   },

   equipRunner: function()
   {
      this.equipRunnerForJumping();
      this.equipRunnerForFalling();
   },

   setTimeRate: function(rate)
   {
      this.timeRate = rate;
      this.timeSystem.setTransducer(function(now)
      {
         return now*snailBait.timeRate;
      });
   },

   equipRunnerForJumping: function()
   {
      var INITIAL_TRACK = 1,
      RUNNER_JUMP_HEIGHT = 120,
      RUNNER_JUMP_DURATION = 1000;

      this.runner.JUMP_HEIGHT = RUNNER_JUMP_HEIGHT;
      this.runner.JUMP_DURATION = RUNNER_JUMP_DURATION;
     

      this.runner.jumping = false;
      this.runner.track = INITIAL_TRACK;

      this.runner.ascendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2,
         AnimationTimer.makeEaseOutEasingFunction(1.1));
      this.runner.descendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2,
         AnimationTimer.makeEaseInEasingFunction(1.1));


      this.runner.jump = function()
      {
         if(this.jumping)
         {
            return;
         }
         this.jumping = true;
         this.runAnimationRate = 0;
         this.verticalLaunchPosition = this.top;
         this.ascendTimer.start(snailBait.timeSystem.calculateGameTime());
      };

      this.runner.stopJumping = function()
      {
         this.descendTimer.stop();
         this.jumping = false;
      };

   },

   armSnails: function()
   {
      var snail,
         snailBombArtist = new SpriteSheetArtist(this.spritesheet,
            this.snailBombCells);

      for(var i=0; i < this.snails.length; ++i)
      {
         snail = this.snails[i];
         snail.bomb = new Sprite('snail bomb', snailBombArtist,[this.snailBombMoveBehaviour]);

         snail.bomb.width = snailBait.SNAILBOMB_CELLS_WIDTH;
         snail.bomb.height = snailBait.SNAILBOMB_CELLS_HEIGHT;

         snail.bomb.top = snail.top + snail.bomb.height/2;
         snail.bomb.left = snail.left + snail.bomb.width/2;
         snail.bomb.visible = false;

         //Snail bombs maintain a reference to their snail
         snail.bomb.snail = snail;
         this.sprites.push(snail.bomb);
      }
   },

   addSpritesToSpriteArray: function()
   {
      for(var i=0; i< this.platforms.length; ++i)
      {
         this.sprites.push(this.platforms[i]);
      }

      for(var i=0; i< this.bats.length; ++i)
      {
         this.sprites.push(this.bats[i]);
      }

      for(var i=0; i< this.bees.length; ++i)
      {
         this.sprites.push(this.bees[i]);
      }

      for(var i=0; i< this.buttons.length; ++i)
      {
         this.sprites.push(this.buttons[i]);
      }

      for(var i=0; i< this.coins.length; ++i)
      {
         this.sprites.push(this.coins[i]);
      }

      for(var i=0; i< this.rubies.length; ++i)
      {
         this.sprites.push(this.rubies[i]);
      }

      for(var i=0; i< this.sapphires.length; ++i)
      {
         this.sprites.push(this.sapphires[i]);
      }

      for(var i=0; i< this.snails.length; ++i)
      {
         this.sprites.push(this.snails[i]);
      }

      this.sprites.push(this.runner);
   },

   putSpriteOnPlatform: function(sprite, platformSprite)
   {
      sprite.top = platformSprite.top - sprite.height;
      sprite.left = platformSprite.left;
      sprite.platform = platformSprite;
   },


	initializeImages: function()
	{
		//this.background.src = 'images/background.png';
		//this.runnerImage.src = 'images/runner.png';
      this.spritesheet.src = 'images/spritesheet.png';
		this.snailAnimatedGifElement.src = 'images/snail.gif';

		this.spritesheet.onload = function (e)
		{
			snailBait.backgroundLoaded();
		};

		this.snailAnimatedGifElement.onload = function ()
		{
			snailBait.loadingAnimationLoaded();
		};
	},

	backgroundLoaded: function()
	{
		var LOADIND_SCREEN_TRANSITION_DURATION = 2000;
		this.fadeOutElements(this.loadingElement, LOADIND_SCREEN_TRANSITION_DURATION);

		setTimeout( function(){
			snailBait.startGame();
			snailBait.gameStarted = true;
		}, LOADIND_SCREEN_TRANSITION_DURATION);
	},

   spritesheetLoaded: function () {
      var LOADING_SCREEN_TRANSITION_DURATION = 2000;

      this.graphicsReady = true;

      this.fadeOutElements(this.loadingElement, 
         LOADING_SCREEN_TRANSITION_DURATION);

      setTimeout ( function () {
         if (! snailBait.gameStarted) {
            snailBait.startGame();
         }
      }, LOADING_SCREEN_TRANSITION_DURATION);
   },

	loadingAnimationLoaded: function()
	{
		if(!this.gameStarted)
		{
			this.fadeInElements(this.snailAnimatedGifElement, this.loadingTitleElement);
		}
	},

	dimControls: function ()
	{
		FINAL_OPACITY = 0.5;
		snailBait.instructionsElement.style.opacity = FINAL_OPACITY;
		snailBait.soundAndMusicElement.style.opacity = FINAL_OPACITY;
	},

	revealCanvas: function ()
	{
		this.fadeInElements(this.canvas);
	},

	revealTopChrome: function()
	{
		this.fadeInElements(this.fpsElement, this.scoreElement);
	},

	revealTopChromeDimmed: function ()
	{
		var DIM = 0.25;

		this.scoreElement.style.display = 'block';
		this.fpsElement.style.display = 'block';

		setTimeout(function ()
		{
			snailBait.scoreElement.style.opacity = DIM;
			snailBait.fpsElement.style.opacity = DIM;
		}, this.SHORT_DELAY);
	},

	revealBottomChrome: function()
	{
		this.fadeInElements(this.soundAndMusicElement, 
			this.instructionsElement, this.copyrightElement);
	},

	revealGame: function()
	{
		var DIM_CONTROLS_DELAY = 5000;

		this.revealTopChromeDimmed();
		this.revealCanvas();
		this.revealBottomChrome();

		setTimeout(function ()
		{
			snailBait.dimControls();
			snailBait.revealTopChrome();
		}, DIM_CONTROLS_DELAY);
	},

	revealInitialToast: function()
	{
		var INITIAL_TOAST_DELAY = 1500,
		INITIAL_TOAST_DURATION = 3000;

		setTimeout(function()
		{
			snailBait.revealToast('Collide with coins and jewels' +
				'Avoid bats and bees', INITIAL_TOAST_DELAY);
		}, INITIAL_TOAST_DURATION);
	},


	startGame: function()
	{
		this.revealGame();
      if(snailBait.mobile)
      {
         this.fadeInElements(snailBait.mobileWelcomeToast);
      }
      else
      {   
		    this.revealInitialToast();
      }
      this.startMusic();
      this.timeSystem.start();
      this.setTimeRate(1.0);
      this.gameStarted = true;
		window.requestAnimationFrame(this.animate);
	},

   startMusic: function()
   {
      var MUSIC_DELAY = 1000;

      setTimeout(function()
      {
         if(snailBait.musicCheckboxElement.checked)
         {
            snailBait.musicElement.play();
         }
         snailBait.pollMusic();
      }, MUSIC_DELAY);
   },

   pollMusic: function()
   {
      var POLL_INTERVAL = 500,
      SOUNDTRACK_LENGTH = 132,
      timerID;

      timerID = setInterval(function()
      {
         if(snailBait.musicElement.currentTime > SOUNDTRACK_LENGTH)
         {
            clearInterval(timerID); //stop polling
            snailBait.restartMusic();
         }
      }, POLL_INTERVAL);
   },

   restartMusic: function()
   {
      snailBait.musicElement.pause();
      snailBait.musicElement.currentTime = 0;
      snailBait.startMusic();
   },

   createAudioChannels: function()
   {
      var channel;

      for(var i=0; i < this.audioChannels.length; ++i)
      {
         channel = this.audioChannels[i];
         if(i !== 0)
         {
            channel.audio = document.createElement('audio');
            channel.audio.addEventListener('loaddata',
               this.soundLoaded, false);
            channel.audio.src = this.audioSprites.currentSrc;
         }
         channel.audio.autobuffer = true;
      }
   },

   soundLoaded: function()
   {
      snailBait.audioSpriteCountdown--;
      if(snailBait.audioSpriteCountdown === 0)
      {
         if(!snailBait.gameStarted && snail.graphicsReady)
         {
            snailBait.startGame();
         }
      }
   },

   playSound: function(sound)
   {
      var channel,
          audio;

      if(this.soundOn)
      {
         channel = this.getFirstAvailableAudioChannel();

         if(!channel)
         {
            if(console)
            {
               console.warn('All audio channels are busy. Cant play sound');
            }
         }
         else
         {
            audio = channel.audio;
            audio.volume = sound.volume;

            this.seekAudio(sound, audio);
            this.playAudio(audio, channel);

             setTimeout(function () {
               channel.playing = false;
               snailBait.seekAudio(sound, audio);
            }, sound.duration);
         }
      }
   },

   getFirstAvailableAudioChannel: function()
   {
      for(var i =0; i < this.audioChannels.length; ++i)
      {
         if(!this.audioChannels[i].playing)
         {
            console.log(i);
            return this.audioChannels[i];
         }
      }
      return null;
   },

   seekAudio: function(sound, audio)
   {
      try
      {
         audio.pause();
         audio.currentTime = sound.position;
      }
      catch(e)
      {
         console.error('Cannot play audio');
      }
   },

   playAudio: function(audio, channel)
   {
      try
      {
         audio.play();
         channel.playing = true;
      }
      catch(e)
      {
         if(console)
         {
            console.error('Cannot play audio');
         }
      }
   },

	animate: function (now)
	{
      now = snailBait.timeSystem.calculateGameTime();
		if(snailBait.paused)
		{
			setTimeout(function ()
			{
				requestAnimationFrame(snailBait.animate);
			}, snailBait.PAUSED_CHECK_INTERVAL);
		}
		else
		{
			snailBait.fps = snailBait.calculateFps(now);
			snailBait.draw(now);
			snailBait.lastAnimationFrameTime = now;
			requestAnimationFrame(snailBait.animate);
		}
	},

	draw: function (now)
	{
		this.setPlatformVelocity();
		this.setOffsets(now);
		this.drawBackground();
		/*this.drawPlatforms();
		this.drawRunner();*/
      this.updateSprites(now);
      this.drawSprites();
      if(snailBait.mobileInstructionsVisible)
      {
         snailBait.drawMobileInstructions();
      }
	},

	setPlatformVelocity: function ()
	{
		this.platformVelocity = this.bgVelocity * 
		this.PLATFORM_VELOCITY_MULTIPLIER;
	},

	setOffsets: function (now)
	{
		this.setBackgroundOffset(now);
      this.setSpriteOffsets(now);
		//this.setPlatformOffset(now);
	},

   setSpriteOffsets: function(now)
   {
      var sprite;
      this.spriteOffset += this.platformVelocity *
         (now-this.lastAnimationFrameTime)/1000;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if('runner' !== sprite.type)
         {
            sprite.hOffset = this.spriteOffset;
         }
      }
   },



	calculateFps: function (now)
	{
		var fps = 1/(now - this.lastAnimationFrameTime) * 1000 * this.timeRate;
		if(now - this.lastFpsUpdateTime > 1000)
		{
			this.lastFpsUpdateTime = now;
			this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
		}
		return fps;
	},

	setBackgroundOffset: function(now)
	{
		this.backgroundOffset += this.bgVelocity*(now-this.lastAnimationFrameTime)/1000;
		if(this.backgroundOffset < 0 || this.backgroundOffset > this.BACKGROUND_WIDTH)
		{
			this.backgroundOffset = 0;
		}
	},

	/*setPlatformOffset: function(now)
	{
		this.platformOffset += this.platformVelocity*(now-this.lastAnimationFrameTime)/1000;
		if(this.platformOffset > 2*this.background.width)
		{
			this.turnLeft();
		}
		else if(this.platformOffset < 0)
		{
			this.turnRight();
		}
	},*/

	turnLeft: function()
	{
		this.bgVelocity = -this.BACKGROUND_VELOCITY;
      this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
      this.runner.artist.cells = this.runnerCellsLeft;
	},

	turnRight: function()
	{
		this.bgVelocity = this.BACKGROUND_VELOCITY;
      this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
      this.runner.artist.cells = this.runnerCellsRight;
	},

	fadeInElements: function ()
	{
		var args = arguments;
		for(var i=0; i < args.length; ++i)
		{
			console.log(args[i]);
			args[i].style.display = 'block';
		}

		setTimeout(function () {
			for(var i=0; i < args.length; ++i)
			{
				args[i].style.opacity = snailBait.OPAQUE;
			}
		}, this.SHORT_DELAY);
	},

	fadeOutElements: function ()
	{
		var args = arguments,
			fadeDuration = args[args.length-1];
		for(var i=0; i < args.length-1; ++i)
		{
			console.log(args[i]);
			args[i].style.opacity = snailBait.TRANSPARENT;
		}

		setTimeout(function () {
		for(var i=0; i < args.length-1; ++i)
		{
			args[i].style.display = 'none';
		}
		}, fadeDuration);
	},

	hideToast: function()
	{
		var TOAST_TRANSITION_DURATION = 450;
		this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
	},

	//This should set the text of the toast 
	//and fade it in over the supplied duration
	startToastTransition: function(text, duration){
		this.toastElement.innerHTML = text;
		this.fadeInElements(this.toastElement);
	},

	drawBackground: function ()
	{
      var BACKGROUND_TOP_IN_SPRITESHEET = 590;
		this.context.translate(-this.backgroundOffset, 0);
		//Initially Onscreen
		this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, 0, 0, this.BACKGROUND_WIDTH, 
         this.BACKGROUND_HEIGHT);
		//Initially Offscreen
		this.context.drawImage(this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT, this.BACKGROUND_WIDTH, 0, this.BACKGROUND_WIDTH, 
         this.BACKGROUND_HEIGHT);
		this.context.translate(this.backgroundOffset, 0);
	},

	drawPlatform: function (data)
	{
		var platformTop = this.calculatePlatformTop(data.track);
		this.context.lineWidth = this.PLATFORM_STROKE_WIDTH;
		this.context.strokeStyle = this.PLATFORM_STROKE_STYLE;
		this.context.fillStyle = data.fillStyle;
		this.context.globalAlpha = data.opacity;

		this.context.strokeRect(data.left, platformTop, data.width, 
			this.PLATFORM_HEIGHT);
		this.context.fillRect(data.left, platformTop, data.width, 
			this.PLATFORM_HEIGHT); 
	},

	drawPlatforms: function ()
	{
		var index;
		this.context.translate(-this.platformOffset, 0);
		for(index =0; index < this.platformData.length; ++index)
		{
			this.drawPlatform(this.platformData[index]);
		}
		this.context.translate(this.platformOffset, 0);
	},

	calculatePlatformTop: function(track)
	{
		if(track === 1)
		{
			return this.TRACK_1_BASELINE; 
		}
		else if (track === 2)
		{
			return this.TRACK_2_BASELINE;
		}
		else if(track === 3)
		{
	 		return this.TRACK_3_BASELINE;
		}
	},


	drawRunner: function()
	{
		this.context.drawImage(this.runnerImage, this.RUNNER_LEFT, 
			this.calculatePlatformTop(this.STARTING_RUNNER_TRACK) - this.runnerImage.height);
	},

   togglePausedStateOfAllBehaviours: function (now) {
      
      var behaviour;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         for (var j=0; j < sprite.behaviours.length; ++j) {
            behaviour = sprite.behaviours[j];

            if (this.paused) {
               if (behaviour.pause) {
                  behaviour.pause(sprite, now);
               }
            }
            else {
               if (behaviour.unpause) {
                  behaviour.unpause(sprite, now);
               }
            }
         }
      }
   },

	togglePaused: function() {
		var now = this.timeSystem.calculateGameTime();
		this.paused = !this.paused;
      this.togglePausedStateOfAllBehaviours(now);
		if(this.paused)
		{
			this.pauseStartTime = now;
		}
		else
		{
			this.lastAnimationFrameTime += (now - this.pauseStartTime);
		}

      if(this.musicOn)
      {
         if(this.paused)
         {
            this.musicElement.pause();
         }
         else
         {
            this.musicElement.play();
         }
      }
	},

	revealToast: function(text, duration){
		var DEFAULT_TOAST_DISPLAY_DURATION = 1000;
		duration = duration || DEFAULT_TOAST_DISPLAY_DURATION;

		this.startToastTransition(text, duration);

		setTimeout(function (e)
		{
			
			snailBait.hideToast();
			
		}, duration);
	},

   // Effects..........................................................

   explode: function (sprite) {
      if ( ! sprite.exploding) {
         if (sprite.runAnimationRate === 0) {
            sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
         }

         sprite.exploding = true;
         snailBait.playSound(snailBait.explosionSound);
      }
   },

	//Sprite functions
	createPlatformSprites: function()
	{
		var sprite, pd,
         PULSE_DURATION = 800,
         PULSE_OPACITY_THRESHOLD = 0.1;

		for(var i=0; i< this.platformData.length; ++i)
		{
			pd = this.platformData[i];
			sprite = new Sprite('platform', this.platformArtist);
			sprite.left = pd.left;
			sprite.width = pd.width;
			sprite.height = pd.height;
			sprite.opacity = pd.opacity;
         sprite.fillStyle = pd.fillStyle;
         sprite.track = pd.track;
         sprite.pulsate = pd.pulsate;
         sprite.button = pd.button;

         sprite.top = this.calculatePlatformTop(pd.track);
         if(sprite.pulsate)
         {
            sprite.behaviours = [new PulseBehaviour(PULSE_DURATION, PULSE_OPACITY_THRESHOLD)];
         }
         this.platforms.push(sprite);
		}
	},

    createRubySprites: function () {
      var ruby,
          SPARKLE_DURATION = 100,
          BOUNCE_DURATION_BASE = 1000,
          BOUNCE_HEIGHT_BASE = 100,
          rubyArtist = new SpriteSheetArtist(this.spritesheet,
                                             this.rubyCells);
   
      for (var i = 0; i < this.rubyData.length; ++i) {
         ruby = new Sprite('ruby', rubyArtist, [new CycleBehaviour(SPARKLE_DURATION),
            new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE*Math.random(),
               BOUNCE_HEIGHT_BASE + BOUNCE_HEIGHT_BASE * Math.random())]);

         ruby.width = this.RUBY_CELLS_WIDTH;
         ruby.height = this.RUBY_CELLS_HEIGHT;
         ruby.value = 200;
         
         ruby.collisionMargin = {
            left: ruby.width/5,
            top: ruby.height/8,
            right: 0,
            bottom: ruby.height/4
         };
         this.rubies.push(ruby);
      }
   },

   createBatSprites: function()
   {
      var bat;
      var BAT_FLAP_DURATION = 200;
      var BAT_FLAP_INTERVAL = 50;

      for(var i=0; i< this.batData.length; ++i)
      {
         bat = new Sprite('bat', new SpriteSheetArtist(
            this.spritesheet, this.batCells), 
         [new CycleBehaviour(BAT_FLAP_DURATION, BAT_FLAP_INTERVAL)]);
         bat.width = this.batCells[1].width;
         bat.height = this.BAT_CELLS_HEIGHT;

         bat.collisionMargin = {
            left: 6, top: 11, right: 4, bottom: 8,
         };
        
         this.bats.push(bat);
      }
   },

   createBeeSprites: function()
   {
      var bee,
          beeArtist,
          BEE_FLAP_DURATION = 100,
          BEE_FLAP_INTERVAL = 30;

      for(var i=0; i< this.beeData.length; ++i)
      {
         bee = new Sprite('bee', new SpriteSheetArtist(
            this.spritesheet, this.beeCells), [new CycleBehaviour(BEE_FLAP_DURATION, BEE_FLAP_INTERVAL)]);
         bee.width = this.BEE_CELLS_WIDTH;
         bee.height = this.BEE_CELLS_HEIGHT;
         
         bee.collisionMargin = {
            left: 10, top: 10, right: 5, bottom: 10,
         };
         this.bees.push(bee);
      }
   },

   createButtonSprites: function()
   {
      var button;

      for(var i=0; i< this.buttonData.length; ++i)
      {
         if(i !== this.buttonData.length - 1)
         {
            button = new Sprite('button', new SpriteSheetArtist(
               this.spritesheet, this.blueButtonCells), [this.paceBehaviour]);
         }
         else
         {
            button = new Sprite('button', new SpriteSheetArtist(
               this.spritesheet, this.goldButtonCells), [this.paceBehaviour]);
         }
         button.width = this.BUTTON_CELLS_WIDTH;
         button.height = this.BUTTON_CELLS_HEIGHT;
         button.velocityX = this.BUTTON_PACE_VELOCITY;
         this.buttons.push(button);
      }
   },

   createCoinSprites: function()
   {
      var coin;
      var BOUNCE_DURATION_BASE = 1000,
          BOUNCE_HEIGHT_BASE = 50,
          BLUE_THROB_DURATION= 100,
          GOLD_THROB_DURATION = 500; 

      for(var i=0; i< this.coinData.length; ++i)
      {
         if(i%2 === 0)
         {
            coin = new Sprite('coin', new SpriteSheetArtist(
               this.spritesheet, this.goldCoinCells),
               [new BounceBehaviour(BOUNCE_DURATION_BASE+BOUNCE_DURATION_BASE * Math.random(), 
                  BOUNCE_HEIGHT_BASE+BOUNCE_HEIGHT_BASE * Math.random()), new CycleBehaviour(GOLD_THROB_DURATION)]);
         }
         else
         {
            coin = new Sprite('coin', new SpriteSheetArtist(
               this.spritesheet, this.blueCoinCells), [new BounceBehaviour(BOUNCE_DURATION_BASE+BOUNCE_DURATION_BASE * Math.random(), 
                  BOUNCE_HEIGHT_BASE+BOUNCE_HEIGHT_BASE * Math.random()), new CycleBehaviour(BLUE_THROB_DURATION)]);
         }
         coin.width = this.COIN_CELLS_WIDTH;
         coin.height = this.COIN_CELLS_HEIGHT;
         coin.value = 50;

         coin.collisionMargin = {
            left:   coin.width/8, top:    coin.height/8,
            right:  coin.width/8, bottom: coin.height/4
         };

         this.coins.push(coin);
      }
   },

   createRunnerSprite: function()
   {
      var RUNNER_LEFT = 50,
          RUNNER_HEIGHT = 53,
          STARTING_RUNNER_TRACK = 1,
          STARTING_RUN_ANIMATION_RATE = 0;

      this.runner = new Sprite('runner', new SpriteSheetArtist(
         this.spritesheet, this.runnerCellsRight), [this.runBehaviour, this.jumpBehaviour, 
                                                      this.collideBehaviour, this.fallBehaviour, this.runnerExplodeBehaviour]);
      this.runner.runAnimationRate = STARTING_RUN_ANIMATION_RATE;
      this.runner.track = STARTING_RUNNER_TRACK;
      this.runner.left = RUNNER_LEFT;
      this.runner.height = RUNNER_HEIGHT;
      this.runner.top = this.calculatePlatformTop(this.runner.track) - 
      RUNNER_HEIGHT;
      
      this.runner.collisionMargin = 
      {
         left: 20,
         top: 15,
         right: 15,
         bottom: 20,
      };
      this.sprites.push(this.runner);
   },

   createSapphireSprites: function () {
      var sapphire,
          SPARKLE_DURATION = 100,
          BOUNCE_DURATION_BASE = 3000,
          BOUNCE_HEIGHT_BASE = 100.
          sapphireArtist = new SpriteSheetArtist(this.spritesheet,
                                                 this.sapphireCells);
   
      for (var i = 0; i < this.sapphireData.length; ++i) {
         sapphire = new Sprite('sapphire', sapphireArtist,[new CycleBehaviour(SPARKLE_DURATION),
            new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE*Math.random(),
               BOUNCE_HEIGHT_BASE + BOUNCE_HEIGHT_BASE * Math.random())]);

         sapphire.width = this.SAPPHIRE_CELLS_WIDTH;
         sapphire.height = this.SAPPHIRE_CELLS_HEIGHT;
         sapphire.value = 100;
         sapphire.collisionMargin = {
            left:   sapphire.width/8, top:    sapphire.height/8,
            right:  sapphire.width/8, bottom: sapphire.height/4
         }; 
         this.sapphires.push(sapphire);
      }
   },

   createSnailSprites: function () {
      var snail,
          snailArtist = new SpriteSheetArtist(this.spritesheet, 
                                              this.snailCells);
   
      for (var i = 0; i < this.snailData.length; ++i) {
         snail = new Sprite('snail', snailArtist, 
            [this.paceBehaviour, this.snailShootBehaviour, 
            new CycleBehaviour(300, 1500)]);

         snail.width  = this.SNAIL_CELLS_WIDTH;
         snail.height = this.SNAIL_CELLS_HEIGHT;
         snail.velocityX = snailBait.SNAIL_PACE_VELOCITY;

         this.snails.push(snail);
      }
   },



   isSpriteInView: function(sprite)
   {
      //only draw sprite if visible on canvas
      return sprite.left + sprite.width > sprite.hOffset && 
      sprite.left < sprite.hOffset + this.canvas.width;
   },
   //Update all sprites first before drawing 
   //all sprites
   updateSprites: function(now)
   {
      var sprite;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if(sprite.visible && this.isSpriteInView(sprite))
         {
            sprite.update(now, this.fps, this.context,
               this.lastAnimationFrameTime);
         }
      }
   },

   detectMobile: function()
   {
      snailBait.mobile = 'ontouchstart' in window;
      console.log(snailBait.mobile);
   },

   fitScreen: function()
   {
      var arenaSize = snailBait.calculateArenaSize(snailBait.getViewportSize());
      snailBait.resizeElementsToFitScreen(arenaSize.width, arenaSize.height);
   },

   getViewportSize: function()
   {
      return { 
         width: Math.max(document.documentElement.clientWidth 
         || window.innerWidth || 0), 
         height: Math.max(document.documentElement.clientHeight 
         || window.innerHeight || 0)};
   },

   drawMobileInstructions: function()
   {
      var cw = this.canvas.width,
          ch = this.canvas.height,
          TOP_LINE_OFFSET = 115,
          LINE_HEIGHT = 40;

      this.context.save();
      this.initializeContextForMobileInstruction();
      this.drawMobileDivider(cw, ch);
      this.drawMobileInstructionsLeft(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
      this.drawMobileInstructionsRight(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
      this.context.restore();
   },

   initializeContextForMobileInstruction: function()
   {
      this.context.textAlign = 'center';
      this.context.textBaseLine = 'middle';
      this.context.font = '26px fantasy';
      this.context.shadowBlur = 2;
      this.context.shadowOffsetX = 2;
      this.context.shadowOffsetY = 2;
      this.context.shadowColor = 'black';
      this.context.fillStyle = 'yellow';
      this.context.strokeStyle = 'yellow';
   },

   drawMobileDivider: function(cw, ch)
   {
      this.context.beginPath();
      this.context.moveTo(cw/2, 0);
      this.context.lineTo(cw/2, ch);
      this.context.stoke();
   },

   drawMobileInstructionsLeft: function(cw, ch, topLineOffset, lineHeight)
   {
      this.context.font = '32px fantasy';
      this.context.fillText('Tap on this side to:', cw/4, ch/2-topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Turn around when running right', cw/4,
         ch/2 - topLineOffset + 2 * lineHeight);
      this.context.fillText('Jump when running left', cw/4,
         ch/2 - topLineOffset + 3 * lineHeight);
   },

   drawMobileInstructionsRight: function(cw, ch, topLineOffset, lineHeight)
   {
      this.context.font = '32px fantasy';
      this.context.fillText('Tap on this side to:', 3*cw/4, ch/2-topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Turn around when running left', 3*cw/4,
         ch/2 - topLineOffset + 2 * lineHeight);
      this.context.fillText('Jump when running right', 3*cw/4,
         ch/2 - topLineOffset + 3 * lineHeight);
      this.context.fillText('Start running right', 3*cw/4,
         ch/2 - topLineOffset + 5 * lineHeight);
   },

   calculateArenaSize: function(viewportSize)
   {
      var DESKTOP_ARENA_WIDTH = 800,
          DESKTOP_ARENA_HEIGHT = 520,
          arenaHeight,
          arenaWidth;
      //maintain aspect ratio
      arenaHeight = viewportSize.width*(DESKTOP_ARENA_HEIGHT/DESKTOP_ARENA_WIDTH);
      if(arenaHeight < viewportSize.height) //Height fits
      {
         arenaWidth = viewportSize.width;
      }
      else //Height does not fit
      {
         arenaHeight = viewportSize.height;
         arenaWidth = arenaHeight*(DESKTOP_ARENA_WIDTH/DESKTOP_ARENA_HEIGHT);
      }
      if(arenaWidth > DESKTOP_ARENA_WIDTH)
      {
         arenaWidth = DESKTOP_ARENA_WIDTH;
      }
      if(arenaHeight > DESKTOP_ARENA_HEIGHT)
      {
         arenaHeight = DESKTOP_ARENA_HEIGHT;
      }
      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },

   resizeElementsToFitScreen: function(arenaWidth, arenaHeight)
   {
      snailBait.resizeElement(document.getElementById('arena'), arenaWidth,
         arenaHeight);
      snailBait.resizeElement(snailBait.mobileWelcomeToast, arenaWidth,
         arenaHeight);
      snailBait.resizeElement(snailBait.mobileStartToast, arenaWidth,
         arenaHeight);
   },

   addTouchEventHandlers: function()
   {
      snailBait.canvas.addEventListener('touchstart', snailBait.touchStart);
      snailBait.canvas.addEventListener('touchend', snailBait.touchEnd);
   },

   touchStart: function(e)
   {
      if(snailBait.playing)
      {
         //Prevent players from inadvertently dragging the game canvas
         e.preventDefault();
      }
   },

   touchEnd: function(e)
   {
      var x = e.changedTouches[0].pageX;
      if(snailBait.playing)
      {
         if(x < snailBait.canvas.width/2)
         {
            snailBait.processLeftTap();
         }
         else if(x > snailBait.canvas.width/2)
         {
            snailBait.processRightTap();
         }
         //Prevent players from double tapping to zoom 
         //when the game is playing
         e.preventDefault();
      }
   },

   processLeftTap: function()
   {
      if(snailBait.runner.direction === snailBait.RIGHT)
      {
         snailBait.turnLeft();
      }
      else
      {
         snailBait.runner.jump();
      }
   },

   processRightTap: function()
   {
      if(snailBait.runner.direction === snailBait.LEFT)
      {
         snailBait.turnRight();
      }
      else
      {
         snailBait.runner.jump();
      }
   },

   platformUnderneath: function (sprite, track) {
      var platform,
          platformUnderneath,
          sr = sprite.calculateCollisionRectangle(), // sprite rect
          pr; // platform rectangle

      if (track === undefined) {
         track = sprite.track; // Look on sprite track only
      }

      for (var i=0; i < snailBait.platforms.length; ++i) {
         platform = snailBait.platforms[i];
         pr = platform.calculateCollisionRectangle();

         if (track === platform.track) {
            if (sr.right > pr.left  && sr.left < pr.right) {
               platformUnderneath = platform;
               break;
            }
         }
      }
      return platformUnderneath;
   },

   putSpriteOnTrack: function(sprite, track) {
      sprite.track = track;
      sprite.top = this.calculatePlatformTop(sprite.track) - 
                   sprite.height;
   },

   resizeElement: function(element, w, h)
   {
      element.style.width = w + 'px';
      element.style.height = h + 'px';
   },

   drawSprites: function()
   {
      var sprite;
      for(var i=0; i < this.sprites.length; ++i)
      {
         sprite = this.sprites[i];
         if(sprite.visible && this.isSpriteInView(sprite))
         {
            this.context.translate(-sprite.hOffset,0);
            sprite.draw(this.context);
            this.context.translate(sprite.hOffset,0);
         }
      }
   }
};

//add some vars for keycodes
var KEY_D = 68,
KEY_LEFT_ARROW = 37,
KEY_RIGHT_ARROW = 39,
KEY_K = 75,
KEY_P = 80;
KEY_J = 74;

//Add Event Listener
window.addEventListener('keydown', function (e){
	var key = e.keyCode;
	if(key === KEY_D || key === KEY_LEFT_ARROW)
	{
		snailBait.turnLeft();
	}
	else if(key === KEY_K || key === KEY_RIGHT_ARROW)
	{
		snailBait.turnRight();
	}
	else if(key === KEY_P)
	{
      snailBait.loseLife();
		snailBait.togglePaused();
	}
   else if(key === KEY_J)
   {
      snailBait.runner.jump();
   }
});

window.addEventListener('blur', function (e){
	snailBait.windowHasFocus = false;
	if(!snailBait.paused)
	{
		snailBait.togglePaused();
	}
});

window.addEventListener('focus', function (e){
	var originalFont = snailBait.toastElement.style.fontSize,
		DIGIT_DISPLAY_DURATION = 1000; //milliseconds
	snailBait.windowHasFocus = true;
	snailBait.countdownInProgress = true;
	if(snailBait.paused)
	{
		snailBait.toastElement.style.font = '128px fantasy';
		if(snailBait.windowHasFocus && snailBait.countdownInProgress)
		{
			snailBait.revealToast('3', DIGIT_DISPLAY_DURATION);
		}
		setTimeout(function (e)
		{
			if(snailBait.windowHasFocus && snailBait.countdownInProgress)
				snailBait.revealToast('2', DIGIT_DISPLAY_DURATION);
			
			setTimeout(function (e)
			{
				if(snailBait.windowHasFocus && snailBait.countdownInProgress)
					snailBait.revealToast('1', DIGIT_DISPLAY_DURATION);
			
				setTimeout(function (e)
				{
					if(snailBait.windowHasFocus && snailBait.countdownInProgress)
					{
						snailBait.togglePaused();
					}
					if(snailBait.windowHasFocus && snailBait.countdownInProgress)
					{
						snailBait.toastElement.style.fontSize = originalFont;
					}
					snailBait.countdownInProgress = false;
				}, DIGIT_DISPLAY_DURATION);
			}, DIGIT_DISPLAY_DURATION);
		}, DIGIT_DISPLAY_DURATION); 
	}
});

var snailBait = new SnailBait();

snailBait.musicCheckboxElement.addEventListener('change', function (e){
   snailBait.musicOn = snailBait.musicCheckboxElement.checked;
   if(snailBait.musicOn)
   {
      snailBait.musicElement.play();
   }
   else
   {
      snailBait.musicElement.pause();
   }
});

snailBait.soundCheckboxElement.addEventListener('change', function (e){
   snailBait.soundOn = snailBait.soundCheckboxElement.checked;
});

snailBait.welcomeStartLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      snailBait.playSound(snailBait.coinSound);
      snailBait.fadeOutElements(snailBait.mobileWelcomeToast, FADE_DURATION);
      snailBait.playing = true;
   });

snailBait.mobileStartLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      snailBait.fadeOutElements(snailBait.mobileStartToast, FADE_DURATION);
      snailBait.mobileInstructionsVisible = false;
      snailBait.playSound(snailBait.coinSound);
      snailBait.playing = true;
   });

snailBait.showHowLink.addEventListener('click',
   function(e)
   {
      var FADE_DURATION = 1000;
      snailBait.fadeOutElements(snailBait.mobileWelcomeToast, FADE_DURATION);
      snailBait.drawMobileInstructions();
      snailBait.revealMobileStartToast();
      snailBait.mobileInstructionsVisible = true;
   });
snailBait.initializeImages();
snailBait.createSprites();
snailBait.createAudioChannels();
snailBait.detectMobile();
if(snailBait.mobile)
{
   //mobile specific stuff - touch handler etc
   snailBait.instructionsElement = document.getElementById('snailbait-mobile-instructions');
   snailBait.addTouchEventHandlers();

   if(/android/i.text(navigator.userAgent))
   {
      snailBait.cannonSound.position = 5.4;
      snailBait.coinSound.position = 4.8;
      snailBait.electricityFlowingSound.position = 0.3;
      snailBait.explosionSound.position = 2.8;
      snailBait.pianoSound.position = 3.5;
      snailBait.thudSound.position = 1.8;
   }
}

snailBait.fitScreen();
window.addEventListener("resize", snailBait.fitScreen());
window.addEventListener("orientationchange", snailBait.fitScreen());







